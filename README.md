# Chess Homework DSW 2022



## Work process

1) Clone the repo
2) Create your dev branch based on your master branch. Example: from ilya-master branch to ilya-dev
3) Write code
4) Add it to git. Only add those files that are required to compile and run the project. That would be the .vcxproj, .cpp/.h and some graphics data later on. Don't add .exe .obj and other files which are generated during the build process.
5) Commit them. Make sure you are still on your dev branch.
6) Push your dev branch to GitLab (origin).
7) Create a merge request from your dev branch to your master branch. Add me (Ilya Neo7k) as the reviewer and the assignee.
8) Your code gets reviewed

    If there are issues the merge request gets discarded. Go to step 3.

    If it's ok I merge and close the request. Go to sleep :)

### HW1

There's a single Knight on the board.  

The user can ask for the possible moves by typing the piece position with a question mark (**a1?**). Only valid moves are displayed.  

The user can move the piece by typing positions of the start and the destination (**a1 b3**). Invalid user input makes no effect.  


### HW2

There are all types of figures.

The board is set just as it would be in the real game.

The user can make moves with white or black figures (in no specific order yet) and perform captures on pieces of the other color.

List of the moves that can be omitted:
- Pawn's two squares advance
- En passant
- Promotion
- Castling

There are no checks and mates yet, the King can be captured just as any other piece.

### HW3

Split your project into 3: engine, console application and tests.

Write tests for your chess engine.

Remember to test both what should and what *should not* happen in a valid game.

For example, figures don't capture others of the same color.

Use your imagination the more tests you have the easier it gets when you develop further.

### HW4

**Engine**

  Make a class for an abstract player.

  Derive from it to make a console player. Use it in the console application.

  Change the logic in the console so it's two players playing against each other.

  Make it possible to start the console with zero, one or two AI players. When there's no AI player you replace him with a console one. You will need to reference the AI library in the console application in order to create the AI player.

**AI**

  Make an own project and reference the engine. Make sure it's a static library.

  Derive from the abstract player and implement its functions.

  Implement an AI which does random valid moves.

  For randomness use [the standard c++ library](https://cplusplus.com/reference/random/)

**Render**

  Create a window showing the board.

  Display the pieces on it.

  Add a text edit where you would enter the moves just like in the console. Then the board updates pieces so you see their new positions.

### HW5

**Engine**

  Add an ability to rollback the last move.

  The game should go back to its previous state.

  Captured pieces should reappear.

  Use Command pattern to do this.

**AI**

  Implement [Minimax](https://www.chessprogramming.org/Minimax) algorithm.

  AI picks the best move possible or a random one if all of them are the same.

**Render**

  Add showing of the available moves.


### HW6

**Engine**

  Implement a LAN multiplayer

  Use Boost.Asio ([here](https://think-async.com/Asio/AsioStandalone.html)'s a standalone header-only version) or any other TCP/IP sockets implementation

  You would have to come up with a way to serialize (convert to binary/text data) commands.

  If you didn't use inheritance this is strainforward - use **reinterpret_cast** to a char pointer and **sizeof**. Contact me if you experience any issues.

**AI**
  
  Make AI consider different moves with several threads. Remember you can't read and write simultaneously in the same place of memory. Synchronize such things with mutexes or think of a way to avoid synchronization at all.

**Render**

  ???
